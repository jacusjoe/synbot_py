# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import requests
import json
from botbuilder.core import ActivityHandler, TurnContext, MessageFactory, CardFactory
from botbuilder.core.teams import TeamsInfo
from botbuilder.schema import Mention, HeroCard, Attachment, CardImage, CardAction, ActionTypes, SuggestedActions


def process_input(
        text
):
    if text == "create ticket":
        return "Success! Ticket is Created!"
    elif text == "delete ticket":
        return "Ticket was deleted"
    elif text == "mentionme":
        return text
    elif text == "find my ip":
        return text
    elif text == "showcards":
        return text
    elif text == "colors":
        return text
    else:
        return "Sorry, I did not understand!"

class EchoBot(ActivityHandler):

    async def on_message_activity(  # pylint: disable=unused-argument
            self, turn_context: TurnContext
    ):
        text = turn_context.activity.text.strip()
        member = await TeamsInfo.get_member(turn_context, turn_context.activity.from_property.id)

        if "</at>" in text:
            text = text[text.find("/at") + 5::]
            text = text.strip()

        text = text.lower()
        response_text = process_input(text)

        if "showcards" == response_text:
            card_attachment = self.create_hero_card()
            await turn_context.send_activity(card_attachment)

        if "mentionme" == response_text:
            mention = Mention(
                mentioned=turn_context.activity.from_property,
                text=f"<at>{member.name}</at>",
                type="mention"
            )
            response_text = MessageFactory.text(f"Hi {mention.text}, your email is {member.email}")
            response_text.entities = [Mention().deserialize(mention.serialize())]

        if "find my ip" == response_text:
            response = requests.get("https://api.ipify.org")
            response_text = response.text
            await turn_context.send_activity(response_text)

        if "colors" == response_text:
            attachment = self.create_options()
            await turn_context.send_activity(attachment)

    def create_options(self) -> Attachment:
        options = HeroCard(title="What is your favorite color?",
                           buttons=[CardAction(type=ActionTypes.im_back,
                                               title="Red",
                                               value="Red"),
                                    CardAction(type=ActionTypes.im_back,
                                               title="Blue",
                                               value="Blue")])
        attachment = MessageFactory.attachment(CardFactory.hero_card(options))
        return attachment

    def create_hero_card(self) -> Attachment:
        herocard = HeroCard(title="This is a hero card",
                            images=[CardImage(
                                url="https://i.picsum.photos/100/200")],
                            buttons=[CardAction(type=ActionTypes.open_url,
                                                title="Open URL",
                                                value="https://www.google.com")])
        card_attachment = MessageFactory.attachment(CardFactory.hero_card(herocard))
        return card_attachment
